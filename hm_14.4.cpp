#include <iostream>

int main()
{
    std::string name;
    std::cout << "Please inout your name: ";
    std::cin >> name;

    std::cout << "The name is: " << name << "\n";
    std::cout << "The length of the name is: " << name.length() << "\n";
    std::cout << "First letter of the name is: " << name[0] << "\n";
    std::cout << "Last letter of the name is: " << name[name.length() - 1] << "\n";
    

}

